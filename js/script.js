// Рекурсия - это процесс, когда функция вызывает саму себя или когда алгоритм решения проблемы использует повторение итерационного процесса посредством собственного вызова.
// В практическом программировании рекурсия используется для решения задач, которые могут быть логически разбиты на меньшие части, похожие на исходную задачу.


function calculateFactorial() {
    let input = prompt('Введіть число:');
    let number = parseInt(input);
  
    // Перевірка коректності введених даних
    while (isNaN(number) || input === '') {
      input = prompt('Введено некоректне значення. Введіть число ще раз:');
      number = parseInt(input);
    }
  
    let factorial = getFactorial(number);
  
    alert(`Факторіал числа ${number} дорівнює ${factorial}`);
  }
  
  function getFactorial(num) {
    if (num === 0) {
      return 1;
    }
  
    return num * getFactorial(num - 1);
  }
  
  calculateFactorial();
  